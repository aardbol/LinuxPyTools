"""
Copyright 2018 Leonardo Malik
This software is provided to you under the EUROPEAN UNION PUBLIC LICENCE v1.2
See https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12 for official translations in other languages
"""
import LinuxFunctions
import re
import subprocess
import os


def get_kde_version():
    """Returns the version of KDE Plasma as a MatchObject instance or False when it's not KDE Plasma"""
    try:
        kde_version = LinuxFunctions.execute("plasmashell --version")

        return re.match(r"plasmashell (\d).(\d+)(.(\d+))?", kde_version, re.I)
    except subprocess.CalledProcessError:
        return False


"""
---------------------------------------------
Ready-to-use configuration functions
---------------------------------------------
"""


def get_monitor_timeout():
    """
    KDE Plasma has a monitor timeout setting for when
    connected to AC or when working on battery. We're returning both

    :return: ac and battery timeouts as a tuple, 0 when not set
    """
    config_file = os.getenv("HOME") + "/.config/powermanagementprofilesrc"
    config_ac_idletime = False
    config_battery_idletime = False
    ac_timeout = 0
    battery_timeout = 0

    with open(config_file) as file:
        for line in file:
            # We skip the low battery config, because that's a different category
            if line.startswith("[AC][DPMSControl]"):
                config_ac_idletime = True
                continue
            elif line.startswith("[Battery][DPMSControl]"):
                config_battery_idletime = True
                continue
            # It's a different setting, so reset the config_idletime var
            elif re.match("\[\w+\]", line):
                config_ac_idletime = False
                config_battery_idletime = False
                continue

            # We're in the AC monitor section
            if config_ac_idletime and line.startswith("idleTime"):
                ac_timeout = int(re.search("idleTime=(\d+)", line)[1])
                config_ac_idletime = False
            # We're in the battery monitor section
            elif config_battery_idletime and line.startswith("idleTime"):
                battery_timeout = int(re.search("idleTime=(\d+)", line)[1])
                config_battery_idletime = False

    return ac_timeout, battery_timeout


def set_monitor_timeout(minutes):
    """
    Set the monitor timeout setting to a new value in seconds for both the AC and battery mode.
    Ask for minutes and convert to seconds.

    :param minutes: an integer in minutes
    :type minutes: int
    """
    seconds = minutes * 60
    config_file = os.getenv("HOME") + "/.config/powermanagementprofilesrc"

    with open(config_file, "r") as file:
        contents = "".join(file.readlines())
        ac_idletime_config_found = True if "[AC][DPMSControl]" in contents else False
        battery_idletime_config_found = True if "[Battery][DPMSControl]" in contents else False

        if ac_idletime_config_found:
            contents = re.sub(r"\[AC\]\[DPMSControl\]\sidleTime=\d+\s+",
                              r"[AC][DPMSControl]\nidleTime={}\n\n".format(seconds),
                              contents,
                              0,
                              re.I)
        else:
            contents = "[AC][DPMSControl]\nidleTime={}\n\n".format(seconds) + contents

        if battery_idletime_config_found:
            contents = re.sub(r"\[Battery\]\[DPMSControl\]\sidleTime=\d+\s+",
                              r"[Battery][DPMSControl]\nidleTime={}\n\n".format(seconds),
                              contents,
                              0,
                              re.I)
        else:
            contents = "[Battery][DPMSControl]\nidleTime={}\n\n".format(seconds) + contents

    with open(config_file, "w") as file:
        file.write(contents)
